import cors from 'cors';
import express from 'express';
import mongoose from 'mongoose';
import bookRouter from './routes/book.routes';
import userRouter from './routes/user.routes';

const app = express();
app.use(cors())
app.use(express.json())

mongoose.connect('mongodb://localhost:27017/biblioteka')
const connection = mongoose.connection
connection.once('open', ()=>{
    console.log('db connected')
})

const router = express.Router();
router.use('/', userRouter );
router.use('/book', bookRouter);


app.use('/', userRouter);

app.use('/book', bookRouter);

app.get('/', (req, res) => res.send('Hello World!'));
app.listen(4000, () => console.log(`Express server running on port 4000`));