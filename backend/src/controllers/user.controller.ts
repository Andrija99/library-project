import express from 'express'
import User from "../models/user"
import Book from "../models/book"

export class UserController{
    
    login = (req: express.Request, res: express.Response)=>{
        let username = req.body.username;
        let password = req.body.password;
      

        User.findOne({'username': username, 'password': password}, (err, user)=>{
            if (err) console.log(err);
            else res.json(user);
        })
    }

    findByUsername = (req: express.Request, res: express.Response)=>{
        let username = req.body.username;

        User.findOne({'username': username}, (err, user)=>{
            if (err) console.log(err);
            else res.json(user);
        })
    }

    findByEmail = (req: express.Request, res: express.Response)=>{
        let email = req.body.email;

        User.findOne({'email': email}, (err, user)=>{
            if (err) console.log(err);
            else res.json(user);
        })
    }

    register = (req: express.Request, res: express.Response)=>{
        let username = req.body.username;
        let password = req.body.password;
        let name = req.body.name;
        let address = req.body.address;
        let contact = req.body.contact;
        let email = req.body.email;
        let pic = req.body.pic;
        let approved = req.body.approved;
        let type = req.body.type;

        var user = new User({
            username: username,
            password : password,
            name : name,
            address : address,
            contact : contact,
            email : email,
            picture : pic,
            approved:approved,
            type:type
        })

        user.save().then(user=>{
            res.status(200).json({'message': 'user added'});

        }).catch(err=>{
            res.status(400).json({'message': 'error'})
        })
    }


    promenaLozinke = (req: express.Request, res: express.Response)=>{
        let username = req.body.username;
        let password = req.body.password;

        User.updateOne({username: username},{$set: { password: password }},(err, resp)=>{
            if(err) console.log(err)
            else res.json({message:'Lozinka promenjena'})
        });


    }

    getRequests = (req: express.Request, res: express.Response)=>{
        let flag = req.body.approved;

        User.find({'approved': flag}, (err, users)=>{
           
            if (err) console.log(err);
            else res.json(users);
        })
    }

    approve = (req: express.Request, res: express.Response)=>{
       
        let username = req.body.username;
        
        User.updateOne({'username': username},{$set: { approved: true }} ,(err, resp)=>{
           
            if (err) console.log(err);
            else res.json({message:'Korisnik prihvacen'})        
        })
    }


    delete = (req: express.Request, res: express.Response)=>{
       
        let username = req.body.username;
        
        User.deleteOne({'username': username} ,(err, resp)=>{
        
            if (err) console.log(err);
            else res.json({message:'Korisnik obrisan'})        
        })
    }

    update = (req: express.Request, res: express.Response)=>{
       
        let address = req.body.address
        let username = req.body.username
        let type = req.body.type
        let contact = req.body.contact
        
        User.updateOne({'username': username},{$set: {address:address, contact : contact, type:type}} ,(err, resp)=>{
        
            if (err) console.log(err);
            else res.json({message:'Korisnik azuriran'})        
        })
    }

    returnBook =(req: express.Request, res: express.Response)=>{

        let username = req.body.username
        let id = req.body.id

        User.updateOne({'username': username,'borrowed.returned':false,"borrowed.id":id},{$set: {"borrowed.$.returned":true}} ,(err, resp)=>{

            if (err) console.log(err);
            else res.json({message:'Knjiga vracena'})        
        })
    }

 
       
}