import e from 'express';
import express from 'express'
import User from '../models/user';
import Book from "../models/book"


export class BookController{

    search = (req: express.Request, res: express.Response)=>{
       
        let name = req.body.name;
        let author = req.body.author;
        
        
        Book.find({$and:[{ name: { $regex: name, $options: "i" }},{author: { $regex: author, $options: "i" }}]},(err, books)=>{
           if(err)console.log(err)
           else res.json(books);
          }
          )
    }


    postComment = (req: express.Request, res: express.Response)=>{
        let username = req.body.username;
        let comment = req.body.comment;
        let rate = req.body.rate;
        let id = req.body.id;
        let date = req.body.date;

      Book.findOneAndUpdate({"id":id,"comments":{$not:{$elemMatch:{"username":username}}}}, {$push: {comments: {comment:comment, rate:rate, date:date, username:username}}},(err, book)=>{
       
            if(!book) res.json({message:'Korisnik vec komentarisao'})
            else res.json({message:'Komentar dodat'})

        });
    }

    getBook = (req: express.Request, res: express.Response)=>{
        let id =req.body.id;
        
        Book.findOne({'id': id}, (err, book)=>{
           
            if (err) console.log(err);
            else res.json(book);
        })
    }

    addBook = (req: express.Request, res: express.Response)=>{

        let id = 0

        var book = new Book({
            id:id,
            name: req.body.name,
            author: req.body.author,
            genre : req.body.genre,
            publisher: req.body.publisher,
            published: req.body.published,
            language: req.body.language,
            cover: req.body.cover,
            stock:0,
            timesTaken:0
        })
       
        
        Book.find({},(err, books)=>{
                id = books.length+1
                book.id = id;
                book.save().then(book=>{
                    res.status(200).json({'message': 'book added'});
                }).catch(err=>{
                    res.status(400).json({'message': 'error'})
                })
           }
        )

       
    
    }

   
    

    borrow = (req: express.Request, res: express.Response)=>{

        let username = req.body.username
        let book = {
            id:req.body.id,
            return_date:req.body.return_date,
            taken_date:req.body.taken_date, 
            author:req.body.author,
            name:req.body.name,
            returned:req.body.returned,
            image:req.body.image

        }

        User.updateOne({'username': username},{$push:{"borrowed":book}} ,(err, resp)=>{
        
            if (err) console.log(err);
            else res.json({message:'Knjiga zaduzena'})        
        })

     

    }

    updateStock =(req: express.Request, res: express.Response)=>{
    let  id = req.body.id
    let inc = req.body.inc
    let taken = req.body.taken

    Book.updateOne({'id': id},{$inc:{stock: inc,timesTaken:taken}} ,(err, resp)=>{
        
        if (err) console.log(err);
        else res.json({message:'Knjiga zaduzena'})        
    })
    }

    top3 = (req: express.Request, res: express.Response)=>{

    Book.find({}).sort({'timesTaken':-1}).limit(3).exec(function(err,books){
          
            if (err) console.log(err);
            else res.json(books);   
       
    })

    }

    getAll = (req: express.Request, res: express.Response)=>{
        
        Book.find({}, (err, books)=>{
           
            if (err) console.log(err);
            else res.json(books);
        })
    }  

    updateBook = (req: express.Request, res: express.Response)=>{
        let id = req.body.id
        let name = req.body.name;
        let author= req.body.author;
        let genre = req.body.genre
        let publisher = req.body.publisher
        let published= req.body.published
        let language = req.body.language
        let cover = req.body.cover
        let stock = req.body.stock

        Book.updateOne({'id': id},{$set:{name: name,author:author,genre:genre, published:published,publisher:publisher,language:language,cover:cover,stock:stock}} ,(err, resp)=>{
        
            if (err) console.log(err);
            else res.json({message:'Knjiga izmenjena'})        
        })
       
    
    }
   

    delete = (req: express.Request, res: express.Response)=>{
        let id = req.body.id

        Book.deleteOne({'id': id} ,(err, resp)=>{
          
            if (err) console.log(err);
            else res.json({message:'Knjiga obrisana'})        
        })
    }

    isAvailable= (req: express.Request, res: express.Response)=>{
        let id = req.body.id
        
        User.find({"borrowed.id":id,"borrowed.returned":false}, (err, users)=>{
         
            if (err) console.log(err);
            else res.json(users);
        })
      
    }
}