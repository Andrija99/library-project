import express from 'express';
import { UserController } from '../controllers/user.controller';

const userRouter = express.Router();

userRouter.route('/login').post(
    (req,res) => new UserController().login(req,res)
)

userRouter.route('/findByUsername').post(
    (req,res) => new UserController().findByUsername(req,res)
)

userRouter.route('/findByEmail').post(
    (req,res) => new UserController().findByEmail(req,res)
)

userRouter.route('/register').post(
    (req,res)=> new UserController().register(req,res)
)

userRouter.route('/promenaLozinke').post(
    (req,res)=> new UserController().promenaLozinke(req,res)
)

userRouter.route('/getRequests').post(
    (req,res)=> new UserController().getRequests(req,res)
)

userRouter.route('/approve').post(
    (req,res)=> new UserController().approve(req,res)
)

userRouter.route('/delete').post(
    (req,res)=> new UserController().delete(req,res)
)

userRouter.route('/update').post(
    (req,res)=> new UserController().update(req,res)
)

userRouter.route('/returnBook').post(
    (req,res)=> new UserController().returnBook(req,res)
)

export default userRouter;