import express from 'express';
import { BookController } from '../controllers/book.controller';

const bookRouter = express.Router();

bookRouter.route('/search').post(
    (req,res) => new BookController().search(req,res)
)

bookRouter.route('/postComment').post(
    (req,res) => new BookController().postComment(req,res)
)

bookRouter.route('/getBook').post(
    (req,res) => new BookController().getBook(req,res)
)

bookRouter.route('/addBook').post(
    (req,res) => new BookController().addBook(req,res)
)


bookRouter.route('/updateBook').post(
    (req,res) => new BookController().updateBook(req,res)
)

bookRouter.route('/borrow').post(
    (req,res) => new BookController().borrow(req,res)
)

bookRouter.route('/updateStock').post(
    (req,res) => new BookController().updateStock(req,res)
)

bookRouter.route('/top3').get(
    (req,res) => new BookController().top3(req,res)
)

bookRouter.route('/delete').post(
    (req,res) => new BookController().delete(req,res)
)

bookRouter.route('/getAll').get(
    (req,res) => new BookController().getAll(req,res)
)

bookRouter.route('/isAvailable').post(
    (req,res) => new BookController().isAvailable(req,res)
)


export default bookRouter;