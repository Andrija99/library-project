import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let User = new Schema(
    {
        username: {
            type: String
        },
        password: {
            type: String
        },
        name: {
            type: String
        },
        address:{
            type: String
        },
        contact: {
            type: String
        },
        email: {
            type: String
        },
        picture: {
            type: String
        },
        approved: {
            type: Boolean
        },
        type: {
            type: String
        },
        borrowed: {
            type: Array<Object>
        }
    }
)

export default mongoose.model('user', User, 'user');