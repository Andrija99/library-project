import mongoose from 'mongoose';
const Schema = mongoose.Schema;

let Book = new Schema(
    {
        id: {
            type: Number,
            auto: true
        },
        name: {
            type: String
        },
        author: {
            type: Array<String>
        },
        genre:{
            type: Array<String>
        },
        publisher: {
            type: String
        },
        published: {
            type: Number
        },
        language: {
            type: String
        },
        cover: {
            type: String
        },
        comments:{
            type:Array<Object>
        },
        stock: {
            type: Number
        },
        timesTaken: {
            type : Number
        }
    }
)

export default mongoose.model('book', Book, 'book');