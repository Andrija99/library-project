import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./components/app/app.component";
import { HeaderComponent } from "./components/header/header.component";
import { LoginComponent } from "./components/login/login.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { PromenaLozinkeComponent } from "./components/promena-lozinke/promena-lozinke.component";
import { RegisterComponent } from "./components/register/register.component";
import { UserComponent } from "./components/user/user.component";
import { SearchComponent } from './components/search/search.component';
import { AddBookComponent } from './components/add-book/add-book.component';
import { FooterComponent } from './components/footer/footer.component';
import { BookdetailsComponent } from './components/bookdetails/bookdetails.component';
import { EnumeratePipe } from "./components/bookdetails/enumerate.pipe";
import { AdminComponent } from './components/admin/admin.component';
import { UpdateDeleteComponent } from './components/update-delete/update-delete.component';
import { ZaduzeneKnjigeComponent } from './components/zaduzene-knjige/zaduzene-knjige.component';
import { Top3Component } from "./components/top3/top3.component";
import { NgImageSliderModule } from 'ng-image-slider';
import { IstorijaZaduzenjaComponent } from './components/istorija-zaduzenja/istorija-zaduzenja.component';
import { AdminLoginComponent } from './components/admin-login/admin-login.component';
import { ReturnDaysComponent } from './components/return-days/return-days.component';
import { UpdateBookComponent } from './components/update-book/update-book.component';
import { BooksComponent } from './components/books/books.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserComponent,
    RegisterComponent,
    PromenaLozinkeComponent,
    HeaderComponent,
    ProfileComponent,
    SearchComponent,
    AddBookComponent,
    FooterComponent,
    Top3Component,
    BookdetailsComponent,
    EnumeratePipe,
    AdminComponent,
    UpdateDeleteComponent,
    ZaduzeneKnjigeComponent,
    IstorijaZaduzenjaComponent,
    AdminLoginComponent,
    ReturnDaysComponent,
    UpdateBookComponent,
    BooksComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgImageSliderModule
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
