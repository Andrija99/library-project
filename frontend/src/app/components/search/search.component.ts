import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from 'src/app/models/Book';
import { BookService } from 'src/app/services/book.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private service: BookService,private router:Router) { }

  ngOnInit(): void {
    this.type = JSON.parse(localStorage.getItem("type"))
  }

  type :String;
  name : String="";
  author : String="";
  books : Book[];
  message : String="";

  details(book){
    localStorage.setItem("book",JSON.stringify(book.id))
    this.router.navigate(['bookdetails'])
  }

 
  search(){
    this.message="";

    this.service.search(this.name,this.author).subscribe((books: Book[])=>{
    
      this.books=books;

      if (books.length==0){
        this.message="Nema rezultata pretrage."
      }

    })


  }

}
