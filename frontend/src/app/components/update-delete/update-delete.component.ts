import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { UserServiceService } from 'src/app/services/user-service.service';

@Component({
  selector: 'app-update-delete',
  templateUrl: './update-delete.component.html',
  styleUrls: ['./update-delete.component.css']
})
export class UpdateDeleteComponent implements OnInit {

  constructor(private servis:UserServiceService) { }

  ngOnInit(): void {
    this.servis.getRequests(true).subscribe((users: User[])=>{
      this.users = users;
    })
  }

  users : User[];

  delete(user){
    let flag = false
    user.borrowed.forEach(element => {
      if(!element.returned)flag = true
    });

    if(!flag){
      this.servis.delete(user.username).subscribe((resp)=>{
        if (resp["message"]=="Korisnik obrisan") {
          window.location.reload()
        } else {
          alert("Error")
        }
      })
    }else alert("Korisnik nije vratio sve knjige!")
    

  }

types =[]
  update(user){
    
    if(user.type != "admin" && user.type != "mod" && user.type != "reader"){
      
      alert("Za tip korisnika je dozvoljeno samo jedan od tri tipa - mod , reader, admin")
      window.location.reload()
    } else {
      this.servis.update(user).subscribe((resp)=>{
        if (resp["message"]=="Korisnik azuriran") {
          window.location.reload()
        } else {
          alert("Error")
        }
      })
    }
  }

}
