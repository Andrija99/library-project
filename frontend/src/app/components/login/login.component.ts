import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { User } from "../../models/User";
import { UserServiceService } from "../../services/user-service.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private service:UserServiceService, private router:Router) { }

  username: String = "";
  password: String = "";
  message: String = "";

  ngOnInit(): void {


  }

  login(){
    this.message = "";


    if (this.username=="" || this.password=="") this.message = "Morate uneti sve podatke";
    else{


      this.service.login(this.username, this.password).subscribe((user: User)=>{
          if(!user || user.type=='admin') this.message = "Pogresni podaci";
          else if(user.approved==false) this.message = "Admin jos nije prihvatio vas zahtev!";
          else {

              localStorage.setItem("username",JSON.stringify(user.username));
              localStorage.setItem("type",JSON.stringify(user.type));

              if(user.type == "reader" || user.type=="mod") 
                  this.router.navigate(['user']).then( () => {
                    window.location.reload();
                  });
              
       }    
      })
    }
  
  }

}
