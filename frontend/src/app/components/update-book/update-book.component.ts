import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from 'src/app/models/Book';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent implements OnInit {

  constructor(private bookService:BookService,private router:Router) { }

  ngOnInit(): void {
    let id = localStorage.getItem("book")
    this.bookService.getBook(id).subscribe((book:Book)=>{
        this.book = book
    })
  }

  onChange(event) {
    this.book.cover = event.target.files[0]['name'];
    this.book.cover =  "/assets/pictures/" + this.book.cover;
  }

  change(book){
    this.bookService.updateBook(book).subscribe((resp)=>{
     if(resp['message']=="Knjiga izmenjena"){
      this.router.navigate(['books']).then( () => {
        window.location.reload();
      });
     }
  })
  }
 book:Book;
 genres :String[]=["horor", "avantura","drama","triler","komedija","romantika","sci-fi"]

}
