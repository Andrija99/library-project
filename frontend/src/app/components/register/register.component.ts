import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { UserServiceService } from 'src/app/services/user-service.service';





@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private service: UserServiceService) { }



  ngOnInit(): void {
    if(JSON.parse(localStorage.getItem("type"))=="admin") this.isAdmin = true
  }

  isAdmin = false;
  message: String="";
  username : String="";
  password: String="";
  passwordConfirm: String="";
  name: String="";
  address: String ="";
  contact: String="";
  email: String="";
  pic: String="";
  type: String="";
  
  onChange(event) {
    this.pic = event.target.files[0]['name'];
    this.pic =  "/assets/pictures" + this.pic;
    
}
 
  register(){

    this.message="";
  
    if (this.username=="" || this.password=="" || this.passwordConfirm=="" || this.name=="" || this.address=="" || this.contact=="" || this.email=="" || this.type==""){
      this.message="Niste uneli sve podatke";
    } else if(!this.password.match("^(?=.*[A-Z])[A-Za-z]{1}(?=.*\d)(?=.*[@$!%*?&+-.])[A-Za-z\d@$!%*?&+=.]")){
      this.message = "Lozinka nije po pravilima";
    } else if(this.password!=this.passwordConfirm) {
      this.message="Lozinke se ne poklapaju";
    }else{
      this.service.findByUsername(this.username).subscribe((user: User)=>{
        if(user){
        
          this.message="Postoji username";
          }else{
            this.service.findByEmail(this.email).subscribe((user: User)=>{
              if(user){
                this.message="Postoji email";
              } else
                {
                  alert(this.pic)
                  if (this.pic==""){
                    this.pic = "/assets/pictures/icon.png"
                  }
                this.service.register(this.username,this.password, this.name, this.address, this.contact, this.email, this.pic, this.type).subscribe((resp)=>{
                if (resp['message']=='user added'){
                  alert('Uspesna registracija')
                  window.location.reload()
                } else {
                  alert('Error')
                }
              })
             }
            })
          }
        })



    }


  }
 
}
