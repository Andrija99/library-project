import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { UserServiceService } from 'src/app/services/user-service.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private servis:UserServiceService) { }

  ngOnInit(): void {
    this.servis.getRequests(false).subscribe((users: User[])=>{
      this.users = users;
    })
  }

  
  users : User[];

  approve(username){
    this.servis.approve(username).subscribe((resp)=>{
      if (resp["message"]=="Korisnik prihvacen") {
        window.location.reload()
      } else {
        alert("Error")
      }
    }
    )
  }
  

}
