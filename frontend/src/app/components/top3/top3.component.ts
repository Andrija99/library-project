import { Component, OnInit, ViewChild } from '@angular/core';
import { NgImageSliderComponent } from 'ng-image-slider';
import { Book } from 'src/app/models/Book';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-top3',
  templateUrl: './top3.component.html',
  styleUrls: ['./top3.component.css']
})
export class Top3Component implements OnInit {

  constructor(private bookService:BookService) { 
   
  }
  @ViewChild('nav') slider: NgImageSliderComponent;
  

  ngOnInit(): void {
    this.bookService.top3().subscribe((books: Book[])=>{
      this.imageObject=[{
        image: books[0].cover,
        thumbImage:books[0].cover,
        title: books[0].name
    }, {
        image: books[1].cover,
        thumbImage:  books[1].cover,
        title: books[1].name
    }, {
        image:  books[2].cover,
        thumbImage:  books[2].cover,
        title: books[2].name
    },{
      image: books[0].cover,
      thumbImage:books[0].cover,
      title: books[0].name
  },]
  
    })
  }


  imageObject = [];


}
