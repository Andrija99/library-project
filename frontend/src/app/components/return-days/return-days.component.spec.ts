import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnDaysComponent } from './return-days.component';

describe('ReturnDaysComponent', () => {
  let component: ReturnDaysComponent;
  let fixture: ComponentFixture<ReturnDaysComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReturnDaysComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReturnDaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
