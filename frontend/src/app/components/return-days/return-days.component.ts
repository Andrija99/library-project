import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-return-days',
  templateUrl: './return-days.component.html',
  styleUrls: ['./return-days.component.css']
})
export class ReturnDaysComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    
     this.days = parseInt(localStorage.getItem("returnDays"))
    
  }

  change(){
  this.message=""
    if(!parseInt(this.changedDays))this.message="Morate uneti broj"
    else {
      localStorage.setItem("returnDays",this.changedDays)
      window.location.reload()
    }

  }
days:any;
changedDays:any;
message:String=''
}
