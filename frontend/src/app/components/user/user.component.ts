import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from 'src/app/models/Book';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private router:Router,private bookService:BookService) {
  
  }

  ngOnInit(): void {
    this.bookService.getAll().subscribe((books:Book[])=>{
      this.count = books.length
      this.books = books;
      this.random()
  })
  }

  calculate(){
    let number = 0 ;
    let sum :number= 0;
    
    this.randomBook.comments.forEach(element=>{
  
      sum=sum + parseInt(element.rate.toString());
      number++
   })
   
    return number!=0?sum/number:"/"
  }

  random(){
    let bookOfTheDay = JSON.parse(localStorage.getItem("randomBook"))
    let today = new Date().toDateString()
    let random;
  

    if(bookOfTheDay==null || bookOfTheDay['date']!=today){
       random = Math.floor(Math.random() * this.count) + 1
      let book = {
        date:today,
        id:random
      }
      localStorage.setItem("randomBook",JSON.stringify(book))
    }
    else random = parseInt(bookOfTheDay["id"])

    this.books.forEach(element=>{
      if(element.id==random)this.randomBook = element;
    })
  }

 count:number;
 books:Book[];
 randomBook:Book=new Book();
}
