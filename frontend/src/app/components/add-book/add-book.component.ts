import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookService } from 'src/app/services/book.service';



@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {

  constructor(private servis: BookService) { }

  ngOnInit(): void {

  }

  onChange(event) {
    this.cover = event.target.files[0]['name'];
    this.cover =  "/assets/pictures/" + this.cover;
  }


  naziv : String="";
  
  authors = []

  genres :String[]=["horor", "avantura","drama","triler","komedija","romantika","sci-fi"]
  genre : Array<String>;

  publisher : String="";
  published : Number;
  language : String="";
  cover : String="";

  message: String="";

  addBook(){
 
    this.message="";
    if (this.naziv=="" || this.genre.length==0 || this.publisher=="" || this.published==0 || this.language==""){
      this.message="Niste uneli sve podatke."
    } else {
      if (this.cover==""){
        this.cover="/assets/pictures/default.jpg"
      } 
    this.servis.addBook(this.naziv, this.authors, this.genre, this.publisher, this.published, this.language, this.cover).subscribe((resp)=>{
      if (resp['message']=='book added'){
        alert("knjiga dodata")
        window.location.reload()
      }else {
        alert("error")
      }
    })
  }
  }

}
