import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from 'src/app/models/Book';
import { User } from 'src/app/models/User';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  constructor(private bookService:BookService,private router:Router) { }

  ngOnInit(): void {
    this.bookService.getAll().subscribe((books:Book[])=>{
        this.books = books
    })
  }

  change(book){
   localStorage.setItem("book",book.id)
   this.router.navigate(['update-book'])

  }

  delete(book){
   
    this.bookService.isAvailable(book.id).subscribe((users:User[])=>{
    
    if(users.length!=0)alert("Knjiga je zaduzena, ne mozete je obrisati!")
    else{
      this.bookService.delete(book.id).subscribe((resp)=>{
        if(resp["message"]=="Knjiga obrisana")window.location.reload()
    })
    }

  })

   

  }
  books:Book[];
  bookToUpdate:Book = null;

}
