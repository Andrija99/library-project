import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User';
import { BookService } from 'src/app/services/book.service';
import { UserServiceService } from 'src/app/services/user-service.service';

@Component({
  selector: 'app-zaduzene-knjige',
  templateUrl: './zaduzene-knjige.component.html',
  styleUrls: ['./zaduzene-knjige.component.css']
})
export class ZaduzeneKnjigeComponent implements OnInit {

  constructor(private servisUser: UserServiceService,private router:Router,private servisBook:BookService) { }

  ngOnInit(): void {
    let username = JSON.parse(localStorage.getItem("username"))
   
    this.servisUser.findByUsername(username).subscribe((user:User)=>{
     
      this.user = user
  
    })
  }

  details(book){
    localStorage.setItem("book",JSON.stringify(book.id))
    this.router.navigate(['bookdetails'])
  }

   parseDate(input) {
    var parts = input.match(/(\d+)/g);
   
    return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
  }
  calculate(book){
    let today = new Date()
    let day = this.parseDate(book.return_date)
    
    let Difference_In_Time = day.getTime()-  today.getTime() ;
    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
    
    return Math.floor(Difference_In_Days);
  }

  razduzi(book){
    
    this.servisBook.updateStock(book.id,1,0).subscribe((resp)=>{
      if(resp['message']!='Knjiga zaduzena') alert('error')
     
    })
    
    this.servisUser.returnBook(book.id,this.user.username).subscribe((resp)=>{
      if(resp['message']=='Knjiga vracena') window.location.reload()
     })
  }

  count(){
    let counter = 0
  
  
    this.user.borrowed.forEach(element=>{
      if(!element.returned)counter++
    })
    return counter
  }

  user : User = new User();
}
