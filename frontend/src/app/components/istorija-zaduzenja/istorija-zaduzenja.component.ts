import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from 'src/app/models/Book';
import { BorrowedBooks } from 'src/app/models/BorrowedBooks';
import { User } from 'src/app/models/User';
import { UserServiceService } from 'src/app/services/user-service.service';

@Component({
  selector: 'app-istorija-zaduzenja',
  templateUrl: './istorija-zaduzenja.component.html',
  styleUrls: ['./istorija-zaduzenja.component.css']
})
export class IstorijaZaduzenjaComponent implements OnInit {

  constructor(private servisUser:UserServiceService, private router:Router) { }
  user:User;

  parseDate(input) {
    var parts = input.match(/(\d+)/g);
   
    return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
  }

  year(date){
    return this.parseDate(date).getFullYear()
  }

  month(date){
    return this.parseDate(date).getMonth() + 1
  }

  day(date){
    return this.parseDate(date).getDate()
  }

  calculate(date1,date2){
    date1 = this.parseDate(date1)
    date2 = this.parseDate(date2)
    
    let Difference_In_Time = date1.getTime()-  date2.getTime() ;
    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
    
    return Math.floor(Difference_In_Days);
  }

  ngOnInit(): void {
 
    let username = JSON.parse(localStorage.getItem("username"))
    this.servisUser.findByUsername(username).subscribe((user:User)=>{
     
      this.user = user
      this.books = this.user.borrowed.filter((obj)=>{
        return obj.returned===true
      });
      this.sortDateReturn()

    })

  }

  sortName(){
    this.books = this.books.sort((n1,n2)=>{
      if(n1.name>n2.name)return 1;
      if(n1.name<n2.name) return -1
      return 0;
    })
  }

  sortAuthor(){
    this.books = this.books.sort((n1,n2)=>{
      if(n1.author[0]>n2.author[0]) return 1;
      if(n1.author[0]<n2.author[0]) return -1
      return 0;
    })
  }

  sortDateReturn(){
    this.books = this.books.sort((n1,n2)=>{return this.calculate(n2.return_date,n1.return_date)})
  }

  sortDateTaken(){
    this.books = this.books.sort((n1,n2)=>{return this.calculate(n2.taken_date,n1.taken_date)})
  }

  sort(){
    switch(this.searchParam){
      case "return_date":this.sortDateTaken();break;
      case "taken_date":this.sortDateReturn();break;
      case "name":this.sortName();break;
      case "author":this.sortAuthor();break;
    }

  }

  details(book){
    
    localStorage.setItem("book",book)
    this.router.navigate(['bookdetails'])
  }

  books:BorrowedBooks[]=[]
  searchParam:String='return_date'
}
