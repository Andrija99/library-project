import { Component, Input, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Book } from 'src/app/models/Book';
import { User } from 'src/app/models/User';
import { BookService } from 'src/app/services/book.service';
import { UserServiceService } from 'src/app/services/user-service.service';
import { EnumeratePipe } from './enumerate.pipe';

@Component({
  selector: 'app-bookdetails',
  templateUrl: './bookdetails.component.html',
  styleUrls: ['./bookdetails.component.css']
})
export class BookdetailsComponent implements OnInit {
  @Input() item :Book;
  
  constructor(private router:Router, private servis:BookService,private servisUser:UserServiceService) { }

  ngOnInit(): void {
   
    this.username = JSON.parse(localStorage.getItem("username"))
    let id = localStorage.getItem("book")
    
    this.servis.getBook(id).subscribe((book:Book)=>{
  
      if (book){
        this.book = book;
       
      }
    })

    this.servisUser.findByUsername(this.username).subscribe((user:User)=>{
      this.user = user
    })
   
  }
user:User = new User();  
message:String=""
username : String=""
book:Book = new Book()
ratings:Number[]=[1,2,3,4,5,6,7,8,9,10]
rate:Number = -1;
comment:String=""

  calculate(){
    let number = 0 ;
    let sum :number= 0;
    
    this.book.comments.forEach(element=>{
  
      sum=sum + parseInt(element.rate.toString());
      number++
   })
   
    return number>0?sum/number:"/"
  }

  formatDate(date){ 
    let array=date.split('T');
    return array[0]+" "+array[1].split(".")[0]
  }

  isTaken(){
    let flag = false
    this.user.borrowed.forEach(element=>{
      if(element.id==this.book.id)flag=true
    })
    return flag
  }

  postComment(){
    this.message = ""
    if(this.comment == "" || this.rate == -1)this.message = "Morate uneti i komentar i ocenu";
    else{
     if(this.isTaken()){
      let id = this.book.id;
      let date = new Date();
     
      this.servis.postComment(this.username,this.comment,this.rate, id, date).subscribe((resp)=>{
       
        if(resp["message"]=="Korisnik vec komentarisao")
        alert("Ne mozete dva puta da komentarisete istu knjigu");
        window.location.reload();
      })
     }else alert("Ne mozete komentarisati, niste citali knjigu")
    }

  }

  zaduzena(id){

    let flag = false;

    this.user.borrowed.forEach(element=>{
      if(element.id==id && !element.returned)flag=true
   })

   return flag
  }

  parseDate(input) {
    
    var parts = input.match(/(\d+)/g);
   
    return new Date(parts[0], parts[1]-1, parts[2]); // months are 0-based
  }

  zaduzi(book){
    
 
      let date = new Date()
      let number = 0
      let flag = false
    
     this.user.borrowed.forEach(element=>{
        if(!element.returned)number++
     
        if(this.parseDate(element.return_date)<date && !element.returned)flag=true
     })
     
     if(number==3)flag=true;
     
     if(flag){
      alert("Ne mozete zaduziti knjigu")
     }else{
      let taken_date = new Date()
      let return_date = new Date()
      let days:any = localStorage.getItem("returnDays")

      if(days!=null) days = parseInt(days)
      else days = 14
     
     
      return_date.setDate(taken_date.getDate()+days)

      this.servis.borrow(this.username,book.id,return_date,taken_date,book.name,book.author,book.cover).subscribe((resp)=>{
       
        if(resp["message"]=="Knjiga zaduzena"){
          this.servis.updateStock(book.id,-1,1).subscribe((resp)=>{
            alert("Zaduzili ste knjigu!");
          })
          
        }
       
        window.location.reload();
      })
     }
    
  }


}
