import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { UserServiceService } from 'src/app/services/user-service.service';




@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private service: UserServiceService) { }

  ngOnInit(): void {
   
    this.username= JSON.parse(localStorage.getItem("username"));
    this.service.findByUsername(this.username).subscribe((user: User)=>{
      this.user = user;
   
    })
  }

  user : User;
  username:String="";


}
