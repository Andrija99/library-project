import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User';
import { UserServiceService } from 'src/app/services/user-service.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  constructor(private service:UserServiceService, private router:Router) { }

  username: String = "";
  password: String = "";
  message: String = "";

  ngOnInit(): void {
    localStorage.setItem("returnDays","14")

  }

  login(){
    this.message = "";


    if (this.username=="" || this.password=="") this.message = "Morate uneti sve podatke";
    else{


      this.service.login(this.username, this.password).subscribe((user: User)=>{
          if(!user || user.type!="admin") this.message = "Ne postoji takav admin";
          else {

              localStorage.setItem("username",JSON.stringify(user.username));
              localStorage.setItem("type",JSON.stringify(user.type));

                  this.router.navigate(['admin']).then( () => {
                    window.location.reload();
                  });
          
       }    
      })
    }
  
  }

}
