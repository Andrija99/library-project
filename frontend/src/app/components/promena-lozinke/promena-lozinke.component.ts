import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User';
import { UserServiceService } from 'src/app/services/user-service.service';




@Component({
  selector: 'app-promena-lozinke',
  templateUrl: './promena-lozinke.component.html',
  styleUrls: ['./promena-lozinke.component.css']
})
export class PromenaLozinkeComponent implements OnInit {

  constructor(private service: UserServiceService,private router:Router) { }

  ngOnInit(): void {
   
    this.username= JSON.parse(localStorage.getItem("username"));
    this.service.findByUsername(this.username).subscribe((user: User)=>{
      this.user = user;

     
    })
  }

  user : User;
  username:String="";
  stara : String="";
  nova : String="";
  potvrda : String="";
  message : String="";

  promenaLozinke() {
    
    this.message="";
    if (this.stara=="" || this.nova=="" || this.potvrda==""){
      this.message="Niste uneli sva polja."
    } else if (this.stara!=this.user.password){
      this.message="Stara lozinka nije dobra."
    } else if (this.nova!=this.potvrda){
      this.message="Potvrda lozinke se ne poklapa sa novom lozinkom."
    } else if(!this.nova.match("^(?=.*[A-Z])[A-Za-z]{1}(?=.*\d)(?=.*[@$!%*?&+-.])[A-Za-z\d@$!%*?&+=.]")){
      this.message="Nova lozinka nije u pravom formatu."
    }else {
      this.service.promenaLozinke(this.username, this.nova).subscribe((resp)=>{
        if (resp['message']=='Lozinka promenjena'){
          alert('Uspesna promena')
          localStorage.removeItem("username")
          localStorage.removeItem("type")
          this.router.navigate(['login']).then( () => {
            window.location.reload();
          });
        } else {
          alert('Error')
        }
      })
    }

  }
}
