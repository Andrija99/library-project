import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { User } from 'src/app/models/User';
import { UserServiceService } from 'src/app/services/user-service.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

 constructor( private router:Router,private service:UserServiceService){
  
 }
  ngOnInit(): void {

    this.type = JSON.parse(localStorage.getItem('type'))
    let username = JSON.parse(localStorage.getItem('username'))

    if(username)
    this.service.findByUsername(username).subscribe((user: User)=>{
      this.user = user;
    })

  }

  logout(){

    localStorage.removeItem("username")
    localStorage.removeItem("type")
    this.router.navigate(['']).then( () => {
      window.location.reload();
    });

  }  
  type:String='';
  user:User=null;
}
