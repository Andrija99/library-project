import { MinValidator } from "@angular/forms";
import { BorrowedBooks } from "./BorrowedBooks";

export class User{
    username : String;
    password : String;
    name : String;
    lastName : String;
    address : String;
    contact : String;
    email : String;
    picture : String;
    approved : Boolean;
    type: String;
    borrowed: Array<BorrowedBooks>=new Array<BorrowedBooks>();
}