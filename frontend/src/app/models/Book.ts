import { Comment } from "./Comment";

export class Book{
    id: number;
    name: String;
    author: Array<String>;
    genre: Array<String>;
    publisher: String;
    published: number;
    language: String;
    cover: String;
    comments:Array<Comment> = new Array()
    stock:Number;
    timesTaken:Number;
}