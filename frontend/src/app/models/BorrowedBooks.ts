export class BorrowedBooks{
    id : Number;
    name : String;
    author : Array<String>;
    returned: boolean;
    taken_date : Date;
    return_date : Date;
    image:String;
}