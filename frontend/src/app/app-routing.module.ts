import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AddBookComponent } from "./components/add-book/add-book.component";
import { AdminLoginComponent } from "./components/admin-login/admin-login.component";
import { AdminComponent } from "./components/admin/admin.component";
import { BookdetailsComponent } from "./components/bookdetails/bookdetails.component";
import { BooksComponent } from "./components/books/books.component";
import { IstorijaZaduzenjaComponent } from "./components/istorija-zaduzenja/istorija-zaduzenja.component";
import { LoginComponent } from "./components/login/login.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { PromenaLozinkeComponent } from "./components/promena-lozinke/promena-lozinke.component";
import { RegisterComponent } from "./components/register/register.component";
import { ReturnDaysComponent } from "./components/return-days/return-days.component";
import { SearchComponent } from "./components/search/search.component";
import { Top3Component } from "./components/top3/top3.component";
import { UpdateBookComponent } from "./components/update-book/update-book.component";
import { UpdateDeleteComponent } from "./components/update-delete/update-delete.component";
import { UserComponent } from "./components/user/user.component";
import { ZaduzeneKnjigeComponent } from "./components/zaduzene-knjige/zaduzene-knjige.component";



const routes: Routes = [
        {path:"",component:Top3Component},
        {path:"user", component:UserComponent}, 
        {path:"register", component:RegisterComponent},
        {path:"login", component:LoginComponent},
        {path:"promenalozinke", component:PromenaLozinkeComponent},
        {path:"profile", component:ProfileComponent},
        {path:"search", component:SearchComponent},
        {path:"addBook", component:AddBookComponent},
        {path:"bookdetails",component:BookdetailsComponent},
        {path:"admin",component:AdminComponent},
        {path:"update-delete",component:UpdateDeleteComponent},
        {path:"zaduzene-knjige",component:ZaduzeneKnjigeComponent},
        {path:"istorija-zaduzenja",component:IstorijaZaduzenjaComponent},
        {path:"admin-login",component:AdminLoginComponent},
        {path:"return-days",component:ReturnDaysComponent},
        {path:"update-book",component:UpdateBookComponent},
        {path:"books",component:BooksComponent}
       
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
