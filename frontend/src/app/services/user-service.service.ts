import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http: HttpClient) { }

 

  login(username, password){
    const data={
      username : username,
      password: password
    }

    return this.http.post('http://localhost:4000/login', data);
  }

  findByUsername(username){
    const data={
      username : username
    }
    return this.http.post('http://localhost:4000/findByUsername', data);   
  }

  findByEmail(email){
    const data={
      email : email
    }
    return this.http.post('http://localhost:4000/findByEmail', data);
  }

  register(username, password, name, address, contact, email, pic, type){
  
    const data={
      username : username,
      password : password,
      name : name,
      address : address,
      contact : contact,
      email : email,
      pic : pic,
      approved:false,
      type : type
    }
    return this.http.post('http://localhost:4000/register', data);
  }

  promenaLozinke(username, nova){
    const data={
      username : username,
      password : nova
    }
    return this.http.post('http://localhost:4000/promenaLozinke', data);

  }

  getRequests(approved){
    const data={
      approved:approved
    }
    
    return this.http.post('http://localhost:4000/getRequests', data);
  }

  approve(username){
    
    const data={
      username:username
    }
    return this.http.post('http://localhost:4000/approve', data);
  }

  delete(username){
   
    const data={
      username:username
    }
    return this.http.post('http://localhost:4000/delete', data);
  }

  update(user){
    const data={
      username: user.username,
      type : user.type,
      address : user.address,
      contact : user.contact
    }
    return this.http.post('http://localhost:4000/update', data);
  }

  returnBook(id,username){
    const data={
      username: username,
      id:id
    }
    return this.http.post('http://localhost:4000/returnBook', data);
  }
}
