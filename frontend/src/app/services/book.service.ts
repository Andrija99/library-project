import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }

  getAll(){
    return this.http.get('http://localhost:4000/book/getAll');
  }

  search(name, author){
    const data={
      name : name,
      author : author
    }

  
    return this.http.post('http://localhost:4000/book/search', data);
  }

  postComment(username, comment, rate, id, date){
    const data={
      username:username,
      comment: comment,
      rate: rate,
      id: id,
      date:date
    }
    return this.http.post('http://localhost:4000/book/postComment', data);
  }

  getBook(id){
    const data={
      id:id
    }
    return this.http.post('http://localhost:4000/book/getBook', data);

  }


  addBook(naziv, authors, genre, publisher, published, language, cover){
    const data={
      name: naziv,
      author: authors,
      genre : genre,
      publisher: publisher,
      published: published,
      language: language,
      cover: cover

    }
    
    
    return this.http.post('http://localhost:4000/book/addBook', data);
  }

  updateBook(book){
    const data={
      id:book.id,
      name: book.name,
      author: book.author,
      genre : book.genre,
      publisher: book.publisher,
      published: book.published,
      language: book.language,
      cover: book.cover,
      stock:book.stock
    }
   
    
    return this.http.post('http://localhost:4000/book/updateBook', data);
  }

  borrow(username,id,return_date,taken_date,name,authors,image){
    const data ={
      id:id,
      username:username,
      return_date:return_date,
      taken_date:taken_date,
      name:name,
      author:authors,
      returned:false,
      image:image
    }
    return this.http.post('http://localhost:4000/book/borrow', data);
  }

  updateStock(id,inc,taken){
    const data ={
      id:id,
      inc:inc,
      taken:taken
    }
    return this.http.post('http://localhost:4000/book/updateStock', data);
  }

  top3(){
   
    return this.http.get('http://localhost:4000/book/top3');
  }
  

  delete(id){
    const data ={
      id:id
    }
    return this.http.post('http://localhost:4000/book/delete', data);
  }

  isAvailable(id){
    const data ={
      id:id
    }
 
    return this.http.post('http://localhost:4000/book/isAvailable', data);
  }
}
